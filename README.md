# protobuf-slides

This presentation was built with [Marp](https://marp.app/)

## macOS setup

Install marp-cli via Brew

```shell
brew install marp-cli
```

Compile the presentation

```shell
marp slides.md
```

Open the presentation

```shell
open slides.html
```

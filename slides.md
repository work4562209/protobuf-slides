---
theme: default
class: invert
---

# Protocol Buffers over HTTP REST

Just a different de/serialization method.

---

# Single source of truth

Schema with `.proto` files can be share via Git Submodule.
Updating to new version is as simple as pulling the latest commits.

```
             ┌───────┐      ┌───────────┐     ┌───────────┐
             │  iOS  │      │  Android  │     │  Backend  │
             └───▲───┘      └─────▲─────┘     └─────▲─────┘
                 │                │                 │
                 │                │                 │
                 │                │                 │
                 │                │                 │
                 │                │                 │
                 │         ┌──────┴───────┐         │
                 └─────────┤ Proto schema ├─────────┘
                           └──────────────┘
```

---

# Definitions in .proto files

```protobuf
syntax = "proto3";

package io.github.lukwol;

enum PhoneTypeDto {
  MOBILE = 0;
  HOME = 1;
  WORK = 2;
}

message PhoneNumberDto {
  string number = 1;
  PhoneTypeDto type = 2;
}

message PersonDto {
  string uuid = 1;
  string name = 2;
  optional string email = 3;

  repeated PhoneNumberDto phones = 4;
}
```

---

# Generated .swift files

```swift
import Foundation
import SwiftProtobuf

struct Io_Github_Lukwol_PersonDto {
  var uuid: String = String()

  var name: String = String()

  var email: String = String()

  var phones: [Io_Github_Lukwol_PhoneNumberDto] = []

  var unknownFields = SwiftProtobuf.UnknownStorage()

  init() {}
}

#if swift(>=5.5) && canImport(_Concurrency)
extension Io_Github_Lukwol_PersonDto: @unchecked Sendable {}
#endif  // swift(>=5.5) && canImport(_Concurrency)

extension Io_Github_Lukwol_PersonDto: 
    SwiftProtobuf.Message,
    SwiftProtobuf._MessageImplementationBase,
    SwiftProtobuf._ProtoNameProviding { ... }
```

---

# Serialization and deserialization

```swift
import Foundation

class AddressBookApi {

    func addressBook() async throws -> Io_Github_Lukwol_AddressBookDto {
        //...
        
        let (data, _) = try await URLSession.shared.data(for: urlRequest)
        
        return try Io_Github_Lukwol_AddressBookDto(serializedData: data)
    }
    
    func createContact(person: Io_Github_Lukwol_PersonDto) async throws {
        //...
        
        urlRequest.httpBody = try person.serializedData()
        
        _ = try await URLSession.shared.data(for: urlRequest)
    }
}
```

---

# Modules in Project

- **App**: Initial setup and Application launch
- **Presentation**: Views, View Models, UI components
- **Domain**: Model, Usecases, Business logic
- **Data**: DTOs, Mappers, Networking, Persistence

![bg left:30%](ios-modules.png)

---

# Modules Dependency Graph

```
                                 ┌─────┐
                   ┌─────────────► App ◄─────────────┐
                   │             └──▲──┘             │
                   │                │                │
                   │                │                │
                   │                │                │
           ┌───────┴──────┐    ┌────┴───┐        ┌───┴──┐
           │ Presentation │    │ Domain │        │ Data │
           └───────▲──────┘    └──┬──┬──┘        └───▲──┘
                   │              │  │               │
                   └──────────────┘  └───────────────┘
```

---

# Actual Model

```swift
public struct Person {
    public let uuid: String
    public let name: String
    public let email: String
    public let phones: [PhoneNumber]
    
    public init(
        uuid: String,
        name: String,
        email: String,
        phones: [PhoneNumber]
    ) {
        self.uuid = uuid
        self.name = name
        self.email = email
        self.phones = phones
    }
}
```

---

# Mapping Data Transfer Objects

```swift
import Domain

enum PersonMapper {
    static func map(dto: Io_Github_Lukwol_PersonDto) -> Person {
        Person(
            uuid: dto.uuid,
            name: dto.name,
            email: dto.email,
            phones: dto.phones.map(PhoneNumberMapper.map)
        )
    }
    
    static func map(model: Person) -> Io_Github_Lukwol_PersonDto {
        Io_Github_Lukwol_PersonDto.with { dto in
            dto.uuid = model.uuid
            dto.name = model.name
            dto.email = model.email
            dto.phones = model.phones.map(PhoneNumberMapper.map)
        }
    }
}
```

---

# Model usage

```swift
public protocol AddressBookService {

    func addressBook() async ->  Result<AddressBook, Error>
    
    func createContact(person: Person) async -> Result<(), Error>

}
```

---

# Payload

Protocol Buffers payloads are in Byte stream formats, which require additional tooling when using HTTP Clients

![bg left:60% 100%](insomnia.png)

---

# Tooling 

- [Protoman app](https://github.com/spluxx/Protoman)
- Encode/decode payload in terminal with Protocol Buffer Compiler [protoc](https://github.com/protocolbuffers/protobuf)
- Use command line client [protoCURL](https://github.com/qaware/protocurl) with human-readable payload
- Ideally find/write simple Insomnia plugin for payload conversion

![bg right:50% 80%](protoman.png)

---

# Comparison to JSON

|       **Protobuf**        |             **JSON**             |
|:-------------------------:|:--------------------------------:|
|      requires schema      |            no schema             |
| reliable code generators  |    manual conversion to code     |
|        type safety        |          no type safety          |
|      more data types      |        simple data types         |
|        byte stream        |          human readable          |
| optional fields in schema | optional fields in documentation |
|      enums in schema      |      enums in documentation      |

---

# Links

- [**Schema**](https://gitlab.com/work4562209/protobuf-definitions) - Protobuf messages definitions of *Address Book* app
- [**iOS App**](https://gitlab.com/work4562209/protobuf-ios) - iOS *Address Book* app
- [**Android App**](https://gitlab.com/work4562209/protobuf-android) - Android *Address Book* app
- [**Ktor App**](https://gitlab.com/work4562209/protobuf-ktor) - Backend *Address Book* app
- [**Slides**](https://gitlab.com/work4562209/protobuf-slides) - This presentation slides code
